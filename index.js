const express = require('express');
const PDFDocument = require('pdfkit');
const app = express();

app.get('/generate-pdf', (req, res) => {
  const doc = new PDFDocument();

  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Disposition', 'attachment; filename=test.pdf');

  doc.pipe(res);

  doc.text('Exemplo de PDF gerado com o PDFKit.');

  doc.end();
});

app.listen(3000, () => {
  console.log('Servidor rodando na porta 3000');
});

//aquió
